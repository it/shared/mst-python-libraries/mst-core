# mst-core

[[_TOC_]]

## Installation

Command Line:

```bash
pip install mst-core --index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
```

requirements.txt

```text
--extra-index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
mst-core
```

## Usage

mst-core contains three components that are used by many other scripts: local_env, setuid, and usage_logger.

```python
from mst.core import local_env, setuid, LogAPIUsage, apps_url

print(apps_url())
print(local_env())
setuid("listmgr")
LogAPIUsage()
```

### local_env

The local_env function will return the current environment type: dev, test, or prod.
This function will check for the presence of the LOCAL_ENV environment variable to use as thsi value, or will fall back to checking the machine hostname for the presence of -d# or -t#, defaulting to prod.

### apps_url

The apps_url function returns an environment aware version of the apps url. If provided with a hostname, it will return the full url, otherwise it will just return the base url.

```python
from mst.core import apps_url

# Assume we are in the dev environment

# returns `apps-dev.mst.edu`
base_url = apps_url()

# returns `https://listmgr.apps-dev.mst.edu`
listmgr_url = apps_url("listmgr")
```

### setuid

The setuid function allows you to switch the uid of the script to a particular user either by name or uid.

### LogAPIUsage

The LogAPIUsage function is used to send metrics back to the apiusage report utility. Just make a call to `LogAPIUsage()` with an optional msg parameter.
